package com.devcamp.j04_javabasic.s50;

public interface Other {
 void other();
 int other(int param);
 String other(String param);
}
