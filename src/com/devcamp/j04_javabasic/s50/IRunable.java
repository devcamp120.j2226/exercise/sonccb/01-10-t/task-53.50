package com.devcamp.j04_javabasic.s50;

public interface IRunable {
 void run();
 void running();
}
